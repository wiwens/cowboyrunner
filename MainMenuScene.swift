//
//  MainMenuScene.swift
//  CowboyRunner
//
//  Created by Wiktor Wielgus on 09.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class MainMenuScene: SKScene {
    
    var playBtn = SKSpriteNode()
    var scoreBtn = SKSpriteNode()
    
    var title = SKLabelNode()
    
    var scoreLabel = SKLabelNode()
    
    var scoreShown = false
    
    override func didMoveToView(view: SKView) {
        initialize()
    }
    
    override func update(currentTime: NSTimeInterval) {
        moveBackgroundsAndGrounds()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            
            if nodeAtPoint(location) == playBtn {
                
                let gameplay = GameplayScene(fileNamed: "GameplayScene")
                gameplay!.scaleMode = .AspectFill
                self.view?.presentScene(gameplay!, transition: SKTransition.doorwayWithDuration(NSTimeInterval(1.5)))
            }
            
            if nodeAtPoint(location) == scoreBtn {
                
                if !scoreShown {
                    showScore()
                } else {
                    scoreShown = false
                    scoreLabel.removeFromParent()
                }
                
            }
        }
    }
    
    func initialize() {
        createBG()
        createGrounds()
        getButtons()
        getLabel()
    }
    
    func createBG() {
        
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "BG")
            bg.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            bg.name = "BG"
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: 0)
            bg.zPosition = 0
            self.addChild(bg)
        }
    }
    
    func createGrounds() {
        
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "Ground")
            bg.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            bg.name = "Ground"
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: -(self.frame.size.height / 2))
            bg.zPosition = 3
            self.addChild(bg)
        }
    }
    
    func moveBackgroundsAndGrounds() {
        
        enumerateChildNodesWithName("BG", usingBlock: ({
            (node, error) in
            
            let bgNode = node as! SKSpriteNode
            
            bgNode.position.x -= 4
            
            if bgNode.position.x < -(self.frame.width) {
                bgNode.position.x += bgNode.size.width * 3
            }
            
        }))
        
        enumerateChildNodesWithName("Ground", usingBlock: ({
            (node, error) in
            
            let groundNode = node as! SKSpriteNode
            
            groundNode.position.x -= 2
            
            if groundNode.position.x < -(self.frame.width) {
                groundNode.position.x += groundNode.size.width * 3
            }
            
        }))
    }
    
    func getButtons() {
        playBtn = self.childNodeWithName("Play") as! SKSpriteNode
        scoreBtn = self.childNodeWithName("Score") as! SKSpriteNode
    }
    
    func getLabel() {
        title = self.childNodeWithName("Title") as! SKLabelNode
        
        title.fontName = "RosewoodStd-Regular"
        title.fontSize = 120
        title.text = "Cowboy Runner"
        
        title.zPosition = 5
        
        let moveUp = SKAction.moveToY(title.position.y + 50, duration: NSTimeInterval(1.3))
        
        let moveDown = SKAction.moveToY(title.position.y - 50, duration: NSTimeInterval(1.3))
        
        let sequence = SKAction.sequence([moveUp, moveDown])
        
        title.runAction(SKAction.repeatActionForever(sequence))
    }
    
    func showScore() {
        
        scoreShown = true
        
        scoreLabel.removeFromParent()
        
        scoreLabel = SKLabelNode(fontNamed: "RosewoodStd-Regular")
        scoreLabel.fontSize = 180
        scoreLabel.text = "\(NSUserDefaults.standardUserDefaults().integerForKey("Highscore"))"
        scoreLabel.position = CGPoint(x: 0, y: -200)
        scoreLabel.zPosition = 9
        
        self.addChild(scoreLabel)
    }
}
