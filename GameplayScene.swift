//
//  GameplayScene.swift
//  CowboyRunner
//
//  Created by Wiktor Wielgus on 06.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class GameplayScene: SKScene, SKPhysicsContactDelegate {
    
    var player = Player()
    
    var obstacles = [SKSpriteNode]()
    
    var canJump = false
    
    var movePlayer = false
    var playerOnObstacle = false
    
    var isAlive = false
    
    var counter = NSTimer()
    
    var scoreLabel = SKLabelNode()
    
    var score = Int(0)
    
    var pausePanel = SKSpriteNode()
    
    var gamePaused = false
    
    var lastUpdateTime = NSTimeInterval(0)
    
    var timeSinceLastAction = NSTimeInterval(0)

    var timeUntilNextAction = NSTimeInterval(4)
    
    override func didMoveToView(view: SKView) {
        initialize()
    }
    
    override func update(currentTime: NSTimeInterval) {
        
        let delta = currentTime - lastUpdateTime
        lastUpdateTime = currentTime
        
        timeSinceLastAction += delta
        
        if timeSinceLastAction >= timeUntilNextAction {
            
            spawnObstacles()
            timeSinceLastAction = NSTimeInterval(0)
            timeUntilNextAction = CDouble(randomBetweenNumbers(2, secondNumber: 6))
        }
        
        if isAlive {
            
            moveBackgroundsAndGrounds()
        }
        
        if movePlayer {
            player.position.x -= 9
        }
        
        checkPlayersBounds()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            
            if nodeAtPoint(location).name == "Restart" {
                let gameplay = GameplayScene(fileNamed: "GameplayScene")
                gameplay!.scaleMode = .AspectFill
                self.view?.presentScene(gameplay!, transition: SKTransition.doorwayWithDuration(1.5))
            }
            
            if nodeAtPoint(location).name == "Quit" {
                let mainMenu = MainMenuScene(fileNamed: "MainMenuScene")
                mainMenu!.scaleMode = .AspectFill
                self.view?.presentScene(mainMenu!, transition: SKTransition.doorwayWithDuration(1.5))
            }
            
            if nodeAtPoint(location).name == "Pause" {
                
                createPausePanel()
            }
            
            if nodeAtPoint(location).name == "Resume" {
                
                gamePaused = false
                
                pausePanel.removeFromParent()
                self.scene?.paused = false
                
                counter = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(1), target: self, selector: #selector(incrementScore), userInfo: nil, repeats: true)
            }
            
            if nodeAtPoint(location).name == "Quit" {
                let mainMenu = MainMenuScene(fileNamed: "MainMenuScene")
                mainMenu!.scaleMode = .AspectFill
                self.view?.presentScene(mainMenu!, transition: SKTransition.doorwayWithDuration(1.5))
            }
        }
        
        if canJump {
            canJump = false
            
            if !gamePaused {
                player.jump()
            }
        }
        
        if playerOnObstacle {
            
            if !gamePaused {
                player.jump()
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody = SKPhysicsBody()
        var secondBody = SKPhysicsBody()
        
        if contact.bodyA.node?.name == "Player" {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.node?.name == "Player" && secondBody.node?.name == "Ground" {
            canJump = true
        }
        
        if firstBody.node?.name == "Player" && secondBody.node?.name == "Obstacle" {
            
            if !canJump {
                movePlayer = true
                playerOnObstacle = true
            }
        }
        
        if firstBody.node?.name == "Player" && secondBody.node?.name == "Cactus" {
            playerDied()
        }
    }
    
    func didEndContact(contact: SKPhysicsContact) {
        var firstBody = SKPhysicsBody()
        var secondBody = SKPhysicsBody()
        
        if contact.bodyA.node?.name == "Player" {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.node?.name == "Player" && secondBody.node?.name == "Obstacle" {
            
            movePlayer = false
            playerOnObstacle = false
        }
    }
    
    func initialize() {
        
        physicsWorld.contactDelegate = self

        isAlive = true
        
        createBG()
        createGrounds()
        createPlayer()
        createObstacles()
        getLabel()
        
        counter = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(1), target: self, selector: #selector(incrementScore), userInfo: nil, repeats: true)
    }
    
    func createPlayer() {
        
        player = Player(imageNamed: "Player 1")
        player.initialize()
        player.position = CGPoint(x: -10, y: 20)
        self.addChild(player)
    }
    
    func createBG() {
        
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "BG")
            bg.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            bg.name = "BG"
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: 0)
            bg.zPosition = 0
            self.addChild(bg)
        }
    }
    
    func createGrounds() {
        
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "Ground")
            bg.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            bg.name = "Ground"
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: -(self.frame.size.height / 2))
            bg.zPosition = 3
            bg.physicsBody = SKPhysicsBody(rectangleOfSize: bg.size)
            bg.physicsBody?.affectedByGravity = false
            bg.physicsBody?.dynamic = false
            bg.physicsBody?.categoryBitMask = ColliderType.Ground
            self.addChild(bg)
        }
    }
    
    func moveBackgroundsAndGrounds() {
        
        enumerateChildNodesWithName("BG", usingBlock: ({
            (node, error) in
            
            let bgNode = node as! SKSpriteNode
            
            bgNode.position.x -= 4
            
            if bgNode.position.x < -(self.frame.width) {
                bgNode.position.x += bgNode.size.width * 3
            }
            
        }))
        
        enumerateChildNodesWithName("Ground", usingBlock: ({
            (node, error) in
            
            let groundNode = node as! SKSpriteNode
            
            groundNode.position.x -= 2
            
            if groundNode.position.x < -(self.frame.width) {
                groundNode.position.x += groundNode.size.width * 3
            }
            
        }))
    }
    
    func createObstacles() {
        
        for i in 0...5 {
            
            let obstacle = SKSpriteNode(imageNamed: "Obstacle \(i)")
            
            switch i {
            case 0:
                obstacle.name = "Cactus"
                obstacle.setScale(0.4)
                obstacle.physicsBody = SKPhysicsBody(rectangleOfSize: obstacle.size)
                
                break
            case 4:
                obstacle.name = "Obstacle"
                obstacle.setScale(0.5)
                
                let offsetX = obstacle.size.width * obstacle.anchorPoint.x
                let offsetY = obstacle.size.height * obstacle.anchorPoint.y
                
                let path = CGPathCreateMutable()
                
                CGPathMoveToPoint(path, nil, 0 - offsetX, 0 - offsetY)
                CGPathAddLineToPoint(path, nil, 1 - offsetX, 12 - offsetY)
                CGPathAddLineToPoint(path, nil, 6 - offsetX, 27 - offsetY)
                CGPathAddLineToPoint(path, nil, 13 - offsetX, 39 - offsetY)
                CGPathAddLineToPoint(path, nil, 18 - offsetX, 45 - offsetY)
                CGPathAddLineToPoint(path, nil, 32 - offsetX, 55 - offsetY)
                CGPathAddLineToPoint(path, nil, 44 - offsetX, 60 - offsetY)
                CGPathAddLineToPoint(path, nil, 57 - offsetX, 63 - offsetY)
                CGPathAddLineToPoint(path, nil, 67 - offsetX, 64 - offsetY)
                CGPathAddLineToPoint(path, nil, 75 - offsetX, 64 - offsetY)
                CGPathAddLineToPoint(path, nil, 85 - offsetX, 62 - offsetY)
                CGPathAddLineToPoint(path, nil, 96 - offsetX, 58 - offsetY)
                CGPathAddLineToPoint(path, nil, 104 - offsetX, 54 - offsetY)
                CGPathAddLineToPoint(path, nil, 111 - offsetX, 48 - offsetY)
                CGPathAddLineToPoint(path, nil, 117 - offsetX, 41 - offsetY)
                CGPathAddLineToPoint(path, nil, 122 - offsetX, 34 - offsetY)
                CGPathAddLineToPoint(path, nil, 127 - offsetX, 21 - offsetY)
                CGPathAddLineToPoint(path, nil, 131 - offsetX, 10 - offsetY)
                CGPathAddLineToPoint(path, nil, 132 - offsetX, 0 - offsetY)
                
                CGPathCloseSubpath(path)
                
                obstacle.physicsBody = SKPhysicsBody(polygonFromPath: path)
                break
            default:
                obstacle.name = "Obstacle"
                obstacle.setScale(0.5)
                obstacle.physicsBody = SKPhysicsBody(rectangleOfSize: obstacle.size)
                break
            }
            
            obstacle.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            obstacle.zPosition = 1
            obstacle.physicsBody?.allowsRotation = false
            obstacle.physicsBody?.categoryBitMask = ColliderType.Obstacle
            
            obstacles.append(obstacle)
        }
    }
    
    func spawnObstacles() {
        
        let index = Int(arc4random_uniform(UInt32(obstacles.count)))
        
        let obstacle = obstacles[index].copy() as! SKSpriteNode
        
        obstacle.position = CGPoint(x: self.frame.width + obstacle.size.width, y: 50)
        
        let move = SKAction.moveToX(-(self.frame.size.width * 2), duration: NSTimeInterval(15))
        let remove = SKAction.removeFromParent()
        
        let sequence = SKAction.sequence([move, remove])
        
        obstacle.runAction(sequence)
        
        self.addChild(obstacle)
    }
    
    func randomBetweenNumbers(firstNumber: CGFloat, secondNumber:CGFloat) -> CGFloat {
        
        return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(firstNumber - secondNumber) + min(firstNumber, secondNumber)
    }
    
    func checkPlayersBounds() {
        
        if isAlive {
            if player.position.x < -(self.frame.size.width / 2) - 35 {
                playerDied()
            }
        }
    }
    
    func getLabel() {
        scoreLabel = self.childNodeWithName("ScoreLabel") as! SKLabelNode
        scoreLabel.fontSize = 130
        scoreLabel.fontName = "RosewoodStd-Regular"
        scoreLabel.text = "0M"
    }
    
    func incrementScore() {
        score += 1
        scoreLabel.text = "\(score)M"
    }
    
    func createPausePanel() {
        
        gamePaused = true
        
        counter.invalidate()
        
        self.scene?.paused = true
        
        pausePanel = SKSpriteNode(imageNamed: "Pause Panel")
        pausePanel.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        pausePanel.position = CGPoint(x: 0, y: 0)
        pausePanel.zPosition = 10
        
        let resume = SKSpriteNode(imageNamed: "Play")
        let quit = SKSpriteNode(imageNamed: "Quit")
        
        resume.name = "Resume"
        resume.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        resume.position = CGPoint(x: -155, y: 0)
        resume.setScale(0.75)
        resume.zPosition = 9
        
        quit.name = "Quit"
        quit.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        quit.position = CGPoint(x: 155, y: 0)
        quit.setScale(0.75)
        quit.zPosition = 9
        
        pausePanel.addChild(resume)
        pausePanel.addChild(quit)
        
        self.addChild(pausePanel)
    }
    
    func playerDied() {
        
        let highscore = NSUserDefaults.standardUserDefaults().integerForKey("Highscore")
        
        if highscore < score {
            NSUserDefaults.standardUserDefaults().setInteger(score, forKey: "Highscore")
        }
        
        player.removeFromParent()
        isAlive = false
        
        for child in children {
            if child.name == "Obstacle" || child.name == "Cactus" {
                child.removeFromParent()
            }
        }
        
        counter.invalidate()
        
        let restart = SKSpriteNode(imageNamed: "Restart")
        let quit = SKSpriteNode(imageNamed: "Quit")
        
        restart.name = "Restart"
        restart.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        restart.position = CGPoint(x: -200, y: -150)
        restart.zPosition = 10
        restart.setScale(0)
        
        quit.name = "Quit"
        quit.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        quit.position = CGPoint(x: 250, y: -150)
        quit.zPosition = 10
        quit.setScale(0)
        
        let scaleUp = SKAction.scaleTo(1, duration: NSTimeInterval(0.5))
        
        restart.runAction(scaleUp)
        quit.runAction(scaleUp)
        
        self.addChild(restart)
        self.addChild(quit)
    }
}
